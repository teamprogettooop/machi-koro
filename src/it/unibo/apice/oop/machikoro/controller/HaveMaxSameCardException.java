package it.unibo.apice.oop.machikoro.controller;

/**
 * Classe per gestire l'eccezione della compera di una copia di una carta oltre
 * al limite.
 */
public class HaveMaxSameCardException extends Exception {

    private static final long serialVersionUID = 1L;
}